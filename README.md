# API Endpoints

<h2 style ="color: rgb(0 147 217) ; text-decoration : underline"> NOTE </h2>

<blockquote style ="border-left:5px solid #FF0000; padding : 10px 0;  font-weight : bold">
    <ul style="list-style : none">
    <h3 style ="color : red">Required Python packages </h3>
        <h5>django-redis is optional if you want caching</h5>
        <li>django</li>
        <li>djangorestframework</li>
        <li>django-cors-headers</li>
        <li>pyjwt[crypto]</li>
        <li>django-redis </li>
        <li>mysqlclient</li>
        <li>django-db-connection-pool</li>
    </ul>
</blockquote>

<h4 style ="color : red"> ***Some Endpoints require Authentication</h4>

### Two base endpoints


<ul>
    <li style="color:red">/admin ( default django endpoint )</li>
    <li style="color:red">/api</li>
</ul>

<h4>/admin</h4>
This endpoint is for the django admin panel where the admin can control the users , groups and other things

<h4 >/api</h4>

This endpoint is for retrieving the data about users , groups, permissions and authentication <br>
<span style="font-weight : bold">It has the following sub-endpoints</span>
<ul>
    <li style="color:red">/users</li>
    <li style="color:red">/groups</li>
    <li style="color:red">/permissions</li>
</ul>


<h4>/users</h4>
Handles requests for user data and has the following sub-endpoints

<ul>
    <li style="color:red">/signup - <span style="color:black"> user Login and returns auth token</span></li>
    <li style="color:red">/login - <span style="color:black"> user Sign Up</span></li>
    <li style="color:red">/all - <span style="color:black"> get list of all users</span></li>
    <li style="color:red">/token-refresh - <span style="color:black"> refresh token to obtain new access token</span></li>
    <li style="color:red">/byid - <span style="color:black"> get user data by email</span></li>
    <li style="color:red">/add-permission - <span style="color:black"> Add a permission to a user</span></li>
    <li style="color:red">/add-group - <span style="color:black"> Add a Group to a user</span></li>
</ul>


<h4>/groups</h4>
Handles requests for group data and has the following sub-endpoints

<ul>
    <li style="color:red">/all - <span style="color:black"> get list of all groups</span></li>
    <li style="color:red">/create - <span style="color:black">create new group</span></li>
    <li style="color:red">/< str:name > - <span style="color:black"> get group details by name</span></li>
    <li style="color:red">/< str:name >/permissions - <span style="color:black">view group permissions</span></li>
    <li style="color:red">/< str:name >/permissions/< int:id >- <span style="color:black"> add permission to group with given permission id</span></li>
</ul>


<h4>/permissions</h4>
Handles requests for permissions and has the following sub-endpoints

<ul>
    <li style="color:red">/all - <span style="color:black">get list of all permissions</span></li>
    <li style="color:red">/create - <span style="color:black">create new permission</span></li>
</ul>





